``color`` - Colors
==================

Color Class
-----------

.. automodule:: color
   :members:

Constants
---------

The color module also contains a few constanst for commonly used colors:

.. py:data:: color.BLACK
.. py:data:: color.WHITE
.. py:data:: color.RED
.. py:data:: color.GREEN
.. py:data:: color.YELLOW
.. py:data:: color.BLUE
.. py:data:: color.MAGENTA
.. py:data:: color.CYAN
